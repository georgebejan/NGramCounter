import java.util.ArrayList;
import java.util.List;
import java.util.Map;

class NGramPrinter {
    void print(final Integer sequenceSize, final Map<String, Integer> ngrams) {
        System.out.println("==== " + sequenceSize + "-grams ===");
        final List<Map.Entry<String, Integer>> entries = getEntriesSortedByValue(ngrams);
        entries.forEach(entry -> System.out.println(entry.getValue() + " " + entry.getKey()));
        System.out.println();
    }

    private List<Map.Entry<String, Integer>> getEntriesSortedByValue(Map<String, Integer> grams) {
        final List<Map.Entry<String, Integer>> entries = new ArrayList<>(grams.entrySet());
        entries.sort((entry1, entry2) -> entry2.getValue().compareTo(entry1.getValue()));

        return entries;
    }
}
