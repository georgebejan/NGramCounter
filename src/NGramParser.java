import java.util.HashMap;
import java.util.Map;

class NGramParser {
    private static final String TOKEN_SPLITTER = " ";

    Map<String, Integer> getNGrams(final Integer sequenceSize, final String text) {
        if (sequenceSize < 1 || sequenceSize > NGramCounter.MAX_SEQUENCE_SIZE) {
            throw new IllegalArgumentException("Contiguous sequence size must be between 1 and " + NGramCounter.MAX_SEQUENCE_SIZE);
        }

        final Map<String, Integer> nGrams = new HashMap<>();
        final String[] tokens = text.split(TOKEN_SPLITTER);

        for (Integer i = 0; i < tokens.length - sequenceSize + 1; i++) {
            final String sequence = mergeTokens(tokens, i, i + sequenceSize);
            nGrams.put(sequence, nGrams.getOrDefault(sequence, 0) + 1);
        }

        return nGrams;
    }

    private String mergeTokens(final String[] tokens, final Integer start, final Integer end) {
        final StringBuilder sb = new StringBuilder();
        for (Integer i = start; i < end; i++)
            sb.append(i > start ? " " : "").append(tokens[i]);

        return sb.toString();
    }
}
