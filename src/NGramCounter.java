import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

public class NGramCounter {
    static final Integer MAX_SEQUENCE_SIZE = 3;

    public static void main(String[] args) {
        final NGramParser nGramParser = new NGramParser();
        final NGramPrinter nGramPrinter = new NGramPrinter();
        final String text = getTextFromFile("input.txt");

        for (Integer n = MAX_SEQUENCE_SIZE; n > 0; n--) {
            final Map<String, Integer> nGrams = nGramParser.getNGrams(n, text);
            nGramPrinter.print(n, nGrams);
        }
    }


    private static String getTextFromFile(final String filename) {
        String result = "";
        try {
            result = new String(Files.readAllBytes(Paths.get("resources" + File.separator + filename)), StandardCharsets.UTF_8);
        } catch (IOException e) {
            System.err.println("Cannot find file \"" + e.getMessage() + "\"");
            System.exit(1);
        }

        return result;
    }
}
