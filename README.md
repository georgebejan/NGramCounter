Write an n-gram counter in Java. An n-gram is a
contiguous sequence of n tokens in a string. For example, the sentence "do
you know that I know you know that I know that ?" has 11 3-grams, 12
2-grams, and 13 1-grams. Since there are multiple instances of some of the
n-grams, there are actually only 8 unique 3-grams, 7 unique 2-grams, and 6
unique 1-ngrams.

Given a plain text document, your program will count up the number of times
each unique n-gram is seen and print out each n-gram and its count in sorted
descending order. Your program should do this for all n-grams where 1 <= n
<= 3. For example, for the sentence above, your program will output
something like this:

==== 3-grams ====

2 you know that  
2 know that I  
2 that I know  
1 do you know  
1 know that ?  
1 know you know  
1 I know you  
1 I know that  

==== 2-grams ====  
3 know that  
2 you know  
2 that I  
2 I know  
1 do you  
1 know you  
1 that ?  

==== 1-grams ====  
4 know  
3 that  
2 you  
2 I  
1 do  
1 ?   